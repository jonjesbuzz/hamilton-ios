//
//  main.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HFAppDelegate class]));
    }
}
