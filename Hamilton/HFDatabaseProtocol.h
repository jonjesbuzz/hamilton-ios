//
//  HFDatabaseProtocol.h
//  Hamilton
//
//  Created by Jonathan Jemson on 4/15/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFUser.h"
#import "HFAccount.h"
#import "HFTransaction.h"
@class HFUser;
@class HFAccount;
@class HFTransaction;
@protocol HFDatabaseProtocol <NSObject>

- (void)loginWithUsername:(NSString*)username password:(NSString*)password completion:(void(^)(NSDictionary*))callback;

- (void)newUserWithDictionary:(NSDictionary*)userDictionary completion:(void(^)(BOOL))callback;
- (void)newAccountWithDictionary:(NSDictionary*)accountDictionary forUser:(HFUser*)user;
- (HFTransaction*)newTransactionFromDictionary:(NSDictionary*)transactionDictionary forAccount:(HFAccount*)account;

- (void)deleteTransaction:(HFTransaction*)transaction forAccount:(HFAccount*)account completion:(void(^)(BOOL))callback;
- (void)deleteAccount:(HFAccount*)account forUser:(HFUser*)user withCompletionHandler:(void(^)(BOOL))callback;

- (void)setBalance:(NSNumber*)balance forAccount:(HFAccount*)account;

@end
