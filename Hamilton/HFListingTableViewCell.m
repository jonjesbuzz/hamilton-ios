//
//  HFListingTableViewCell.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/20/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFListingTableViewCell.h"

@implementation HFListingTableViewCell
@synthesize detailTextLabel;
@synthesize textLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutSubviews];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
