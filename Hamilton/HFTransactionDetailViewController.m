//
//  HFTransactionDetailViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/14/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFTransactionDetailViewController.h"

@interface HFTransactionDetailViewController ()

@end

@implementation HFTransactionDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.transaction.name;
    transactionNameLabel.text = self.transaction.name;
    if (self.transaction.amount.doubleValue < 0) {
        transactionAmountLabel.textColor = [UIColor redColor];
        transactionActionLabel.text = @"You spent";
    }
    NSNumber *absValAmt = [NSNumber numberWithDouble:fabs(self.transaction.amount.doubleValue)];
    transactionAmountLabel.text = [self getCurrencyForNumber:absValAmt];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterNoStyle;
    df.dateStyle = NSDateFormatterLongStyle;
    transactionDateLabel.text = [df stringFromDate:self.transaction.transactionDate];
    categoryLabel.text = self.transaction.category;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSString*)getCurrencyForNumber:(NSNumber*)number
{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyStyle stringFromNumber:number];
}

@end
