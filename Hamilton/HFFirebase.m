//
//  HFFirebase.m
//  Hamilton
//
//  Created by Jonathan Jemson on 4/15/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFFirebase.h"
#define FIRST_NAME @"firstName"
#define LAST_NAME @"lastName"
#define NET_WORTH @"netWorth"
#define ACCOUNTS @"accounts"


@implementation HFFirebase
- (void)loginWithUsername:(NSString*)username password:(NSString*)password completion:(void(^)(NSDictionary*))callback
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    NSString *userURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@", username];
    NSString *loginURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/login/%@/password", username];
    Firebase *loginFirebase = [[Firebase alloc] initWithUrl:loginURL];
    [loginFirebase observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        id dbPassword = snapshot.value;
        if ([dbPassword class] == [NSNull class] || ![dbPassword isEqualToString:password]) {
            userInfo[@"loggedIn"] = @NO;
            if (callback != nil) {
                callback(userInfo);
            }
        }
        else {
            Firebase *userFirebase = [[Firebase alloc] initWithUrl:userURL];
            [userFirebase observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                id dbUser = snapshot.value;
                //NSLog(@"%@", dbUser);
                if ([dbUser class] != [NSNull class]) {
                    userInfo[@"netWorth"] = [NSNumber numberWithDouble:0.0];
                    userInfo[@"loggedIn"] = @YES;
                    userInfo[@"firstName"] = dbUser[FIRST_NAME];
                    userInfo[@"lastName"] = dbUser[LAST_NAME];
                    NSDictionary *accountsRef = dbUser[ACCOUNTS];
                    userInfo[@"accounts"] = [[NSMutableArray alloc] initWithCapacity:[accountsRef count]];
                    for (NSString *acctRef in accountsRef)
                    {
                        HFAccount *currAccount = [[HFAccount alloc] initWithAccountDictionary: accountsRef[acctRef]];
                        [userInfo[@"accounts"] addObject:currAccount];
                        userInfo[@"netWorth"] = [NSNumber numberWithDouble:((NSNumber*)userInfo[@"netWorth"]).doubleValue + currAccount.balance.doubleValue];
                    }
                    if (callback != nil) {
                        callback(userInfo);
                        [[NSNotificationCenter defaultCenter] postNotificationName:HFFirebaseUpdatedNotification object:nil];
                    }
                    
                }
            }];
        }
    }];

}

- (void)newUserWithDictionary:(NSDictionary*)userDictionary completion:(void(^)(BOOL))callback
{
    NSString *userURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/"];
    NSString *loginURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/login/"];
    Firebase *loginFirebase = [[[Firebase alloc] initWithUrl:loginURL] childByAppendingPath:userDictionary[@"username"]];
    [loginFirebase observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot){
        if ([snapshot.value class] != [NSNull class]) {
            if (callback != nil) {
                callback(NO);
            }
        }
        else {
            Firebase *userFirebase = [[[Firebase alloc] initWithUrl:userURL] childByAppendingPath:userDictionary[@"username"]];
            [[loginFirebase childByAppendingPath:@"password"] setValue:userDictionary[@"password"]];
            NSDictionary *payload = @{@"firstName": userDictionary[@"firstName"],
                                      @"lastName": userDictionary[@"lastName"],
                                      @"email": userDictionary[@"email"]};
            [userFirebase setValue:payload];
            if (callback != nil)
                callback(YES);
        }
    }];
}
- (void)newAccountWithDictionary:(NSDictionary*)accountDictionary forUser:(HFUser*)user
{
    NSString *accountURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@/accounts/%@", [HFSession sharedInstance].currentUser.username, accountDictionary[@"accountName"]];
    HFAccountType accountType = (HFAccountType)(((NSNumber*)accountDictionary[@"accountType"]).intValue);
    NSString *typeString = [HFAccount typeStringForAccountType:accountType];
    [accountDictionary setValue:typeString forKey:@"accountType"];
    
    Firebase *accountFirebase = [[Firebase alloc] initWithUrl:accountURL];
    NSDictionary *accountPayload = @{@"balance": accountDictionary[@"initialBalance"],
                                     @"fullName": accountDictionary[@"accountName"],
                                     @"name": accountDictionary[@"accountName"],
                                     @"displayName":accountDictionary[@"accountName"],
                                     @"interestRate":accountDictionary[@"interestRate"],
                                     @"transactions": @{},
                                     @"type": accountDictionary[@"accountType"]
                                     };
    [accountFirebase setValue:accountPayload];
    NSNumber *initialBalance = accountDictionary[@"initialBalance"];
    if (initialBalance.intValue != 0) {
        Firebase *newTransaction = [[accountFirebase childByAppendingPath:@"transactions"] childByAutoId];
        NSDictionary *transactionPayload = @{@"amount": accountDictionary[@"initialBalance"],
                                             @"sourceCategory": @"Initial Balance",
                                             @"source": @"Initial Balance"};
        [newTransaction setValue:transactionPayload];
        NSDate *now = [NSDate date];
        NSNumber *nowInterval = [NSNumber numberWithLong:[now timeIntervalSince1970] * ANDROID_DATE_SCALE];
        [[newTransaction childByAppendingPath:@"transactionDate"] setValue:nowInterval];
        [[newTransaction childByAppendingPath:@"dateEntered"] setValue:nowInterval];
    }
}
- (HFTransaction*)newTransactionFromDictionary:(NSDictionary*)transactionDictionary forAccount:(HFAccount*)account
{
    if (transactionDictionary[@"source"] != nil || transactionDictionary[@"reason"] != nil) {
        NSString *transactionsURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@/accounts/%@/transactions", [HFSession sharedInstance].currentUser.username, account.accountName];
        Firebase *transactions = [[[Firebase alloc] initWithUrl:transactionsURL] childByAutoId];
        [transactions setValue:transactionDictionary];
        return [[HFTransaction alloc] initWithTransactionDictionary:transactionDictionary transactionID:[transactions name]];
    }
    return nil;
}
- (void)deleteTransaction:(HFTransaction*)transaction forAccount:(HFAccount*)account completion:(void(^)(BOOL))callback
{
    NSString *transactionsURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@/accounts/%@/transactions/%@", [HFSession sharedInstance].currentUser.username, account.accountName, transaction.transactionID];
    Firebase *transactionBase = [[Firebase alloc] initWithUrl:transactionsURL];
    [transactionBase removeValueWithCompletionBlock:^(NSError *error, Firebase *ref) {
        if (error == nil && callback != nil)
            callback(YES);
        else if (error != nil && callback != nil)
            callback(NO);
    }];
}
- (void)deleteAccount:(HFAccount*)account forUser:(HFUser*)user withCompletionHandler:(void(^)(BOOL))callback
{
    NSString *accountURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@/accounts/%@", [HFSession sharedInstance].currentUser.username, account.accountName];
    Firebase *accountFirebase = [[Firebase alloc] initWithUrl:accountURL];
    [accountFirebase removeValueWithCompletionBlock:^(NSError *error, Firebase *ref) {
        if (error == nil && callback != nil)
            callback(YES);
        else if (error != nil && callback != nil)
            callback(NO);
    }];
}
- (void)setBalance:(NSNumber*)balance forAccount:(HFAccount*)account
{
    NSString *accountURL = [NSString stringWithFormat:@"https://hamiltontech.firebaseio.com/users/%@/accounts/%@", [HFSession sharedInstance].currentUser.username, account.accountName];
    Firebase *accountFirebase = [[Firebase alloc] initWithUrl:accountURL];
    [[accountFirebase childByAppendingPath:@"balance"] setValue:balance];
}
@end
