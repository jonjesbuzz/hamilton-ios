//
//  HFSession.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFSession.h"
@implementation HFSession

@synthesize currentUser;

static HFSession *sharedSingleton = nil;

+ (HFSession*) sharedInstance {
    @synchronized(self) {
        if (sharedSingleton == nil) {
            sharedSingleton = [[HFSession alloc] init];
        }
    }
    return sharedSingleton;
}
+ (id) allocWithZone:(NSZone *) zone {
    @synchronized(self) {
        if (sharedSingleton == nil) {
            sharedSingleton = [super allocWithZone:zone];
            return sharedSingleton;
        }
    }
    return nil;
}
- (void)destroySession
{
    sharedSingleton = nil;
}

@end