//
//  HFFirebase.h
//  Hamilton
//
//  Created by Jonathan Jemson on 4/15/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFFirebase.h"
#import "HFDatabaseProtocol.h"
#import <Firebase/Firebase.h>

#import "HFAccount.h"
#import "HFTransaction.h"
#import "HFUser.h"

@interface HFFirebase : NSObject <HFDatabaseProtocol>

@end
