//
//  HFListingTableViewCell.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/20/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFListingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
