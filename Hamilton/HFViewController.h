//
//  HFViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFLoginViewController.h"

@interface HFViewController : UIViewController <HFLoginViewControllerDelegate>
{
    
}
@end
