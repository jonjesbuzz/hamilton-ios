//
//  HFTransactionDetailViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/14/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFTransaction.h"
@interface HFTransactionDetailViewController : UIViewController
{
    IBOutlet UILabel *transactionNameLabel;
    IBOutlet UILabel *transactionActionLabel;
    IBOutlet UILabel *transactionAmountLabel;
    IBOutlet UILabel *transactionDateLabel;
    IBOutlet UILabel *categoryLabel;
}
@property(strong, nonatomic) HFTransaction *transaction;
@end
