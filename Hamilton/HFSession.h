//
//  HFSession.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFUser.h"

@interface HFSession : NSObject {
    
}
+ (HFSession*) sharedInstance;
- (void)destroySession;
@property(strong, nonatomic) HFUser *currentUser;
@end