//
//  HFRegisterViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFRegisterViewController.h"

@interface HFRegisterViewController ()
- (IBAction)dismissRegisterView:(id)sender;
- (BOOL)validateEmail: (NSString *) candidate;
@end

@implementation HFRegisterViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    self.view.tintColor = HamiltonColor;
    firstNameField.delegate = lastNameField.delegate = usernameField.delegate = emailField.delegate = passwordField.delegate = verifyPasswordField.delegate = self;
    [firstNameField performSelector:@selector(becomeFirstResponder) withObject:firstNameField afterDelay:0.05];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == firstNameField) {
        [firstNameField resignFirstResponder];
        [lastNameField becomeFirstResponder];
    }
    else if (textField == lastNameField) {
        [lastNameField resignFirstResponder];
        [usernameField becomeFirstResponder];
    }
    else if (textField == usernameField) {
        [usernameField resignFirstResponder];
        [emailField becomeFirstResponder];
    }
    else if (textField == emailField) {
        [emailField resignFirstResponder];
        [passwordField becomeFirstResponder];
    }
    else if (textField == passwordField) {
        [passwordField resignFirstResponder];
        [verifyPasswordField becomeFirstResponder];
    }
    else if (textField == verifyPasswordField) {
        [verifyPasswordField resignFirstResponder];
        [self registerNewUser:textField];
    }
    return YES;
}
- (IBAction)registerNewUser:(id)sender {
    BOOL passwordsValid = [passwordField.text isEqualToString:verifyPasswordField.text] && [passwordField.text length] >= 6;
    BOOL emailValid = [self validateEmail:emailField.text];
    if (!passwordsValid) {
        [[[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Make sure your passwords match and are at least six characters long." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else if (!emailValid) {
        [[[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Make sure you entered a valid email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else if ([usernameField.text isEqualToString:@""])
    {
        UIAlert(@"Username Required", @"Please enter a valid username.");
    }
    else if ([firstNameField.text isEqualToString:@""] || [lastNameField.text isEqualToString:@""])
    {
        UIAlert(@"Name Required", @"You must enter your first and last name.");
    }
    else {
        NSDictionary *userDictionary = @{@"username": usernameField.text, @"password": passwordField.text, @"email": emailField.text, @"firstName": firstNameField.text, @"lastName": lastNameField.text};
        __block __weak HFRegisterViewController *weakSelf = self;
        [HFUser createNewUserWithUserDictionary:userDictionary callbackBlock:^(BOOL userCreated) {
            if (!userCreated) {
                [[[UIAlertView alloc] initWithTitle:@"Username exists" message:@"Someone else is using that username... Choose another username." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Welcome %@!", firstNameField.text] message:@"You can now login to Hamilton!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                [weakSelf dismissRegisterView:sender];
            }

        }];
    }
}
- (BOOL)validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (IBAction)dismissRegisterView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.presentingPopoverController != nil && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.presentingPopoverController dismissPopoverAnimated:YES];
    }
}

@end
