//
//  HFLoginViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFLoginViewController.h"
#import "HFUser.h"
#import "HFSession.h"
#import <Firebase/Firebase.h>



@interface HFLoginViewController () {
    Firebase *firebase;
}
@end
@implementation HFLoginViewController
@synthesize delegate = _delegate;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    self.view.tintColor = HamiltonColor;
    [usernameField performSelector:@selector(becomeFirstResponder) withObject:usernameField afterDelay:0.05];
    usernameField.delegate = passwordField.delegate = self;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */
- (IBAction)loginToHamilton:(id)sender {
    HFUser *user = [[HFUser alloc] initWithUsername:usernameField.text];
    __block __weak UITextField *weakPWField = passwordField;
    __block __weak HFUser *weakUser = user;
    [user loginWithPassword:passwordField.text callbackBlock:^(BOOL loggedIn){
        if (!loggedIn) {
            [[[UIAlertView alloc] initWithTitle:@"Try Again" message:@"Your username or password was incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [weakPWField becomeFirstResponder];
            
        }
        else {
            [HFSession sharedInstance].currentUser = weakUser;
            [self dismissLoginSheet:sender];
        }

    }];
    }
- (IBAction)dismissLoginSheet:(id)sender {
    //NSLog(@"LessGo : %@", [HFSession sharedInstance].currentUser);
    __block __weak HFLoginViewController *weakSelf = self;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
    [self dismissViewControllerAnimated:YES completion:^() {
        [weakSelf.delegate didFinishHandlingUser:[HFSession sharedInstance].currentUser.loggedIn];
    }];
    } else
    {
        [self.presentingPopoverController dismissPopoverAnimated:YES];
        [weakSelf.delegate didFinishHandlingUser:[HFSession sharedInstance].currentUser.loggedIn];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == usernameField)
    {
        [usernameField resignFirstResponder];
        [passwordField becomeFirstResponder];
    }
    else
    {
        [passwordField resignFirstResponder];
        [self loginToHamilton:textField];
    }
    return YES;
}

@end
