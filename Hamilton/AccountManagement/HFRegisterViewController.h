//
//  HFRegisterViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFRegisterViewController : UITableViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *firstNameField;
    IBOutlet UITextField *lastNameField;
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *emailField;
    IBOutlet UITextField *passwordField;
    IBOutlet UITextField *verifyPasswordField;
    
}
@property(nonatomic, weak) UIPopoverController *presentingPopoverController;
@end
