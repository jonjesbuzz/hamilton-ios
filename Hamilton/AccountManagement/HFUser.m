//
//  HFUser.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFUser.h"
#import "HFAccount.h"
#import "HFTransaction.h"
#import DATABASE_TYPE_HEADER
@interface HFUser()
@property(strong, nonatomic) Firebase *userFirebase;
@end

@implementation HFUser
@synthesize username = _username;
@synthesize loggedIn = _loggedIn;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize accounts = _accounts;
@synthesize netWorth = _netWorth;
- (HFUser*) initWithUsername:(NSString *)username
{
    _username = username;
    _loggedIn = NO;
    _database = [[DATABASE_TYPE alloc] init];
    return self;
}

- (void)loginWithPassword:(NSString*)password callbackBlock:(void(^)(BOOL))callback
{
    [_database loginWithUsername:self.username password:password completion:^(NSDictionary *userInfo) {
        if ([userInfo[@"loggedIn"] isEqualToValue:@NO]) {
            if (callback != nil) {
                callback(NO);
            }
        }
        else {
            _loggedIn = ((NSNumber*)userInfo[@"loggedIn"]).boolValue;
            _accounts = userInfo[@"accounts"];
            _firstName = userInfo[@"firstName"];
            _lastName = userInfo[@"lastName"];
            _netWorth = userInfo[@"netWorth"];
            if (callback != nil)
                callback(YES);
        }
    }];
}
/* Dictionary should contain username, firstName, lastName, password, email */
+ (void) createNewUserWithUserDictionary:(NSDictionary*) userDictionary callbackBlock:(void(^)(BOOL))callback
{
    [[[DATABASE_TYPE alloc] init] newUserWithDictionary:userDictionary completion:callback];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"User : %@ -> logged in: %@", _username, (_loggedIn) ? @"Yes" : @"No"];
}
-(NSArray*)reportArrayFromDate:(NSDate*)startDate toDate:(NSDate*)endDate
{
    NSMutableArray *reportArray = [[NSMutableArray alloc] init];
    for (HFAccount *account in _accounts)
    {
        for (HFTransaction *transaction in account.transactions)
        {
            if ([HFUser date:transaction.transactionDate isBetweenDate:startDate andDate:endDate])
            {
                if (transaction.amount.doubleValue < 0)
                    [reportArray addObject:transaction];
            }
        }
    }
    return reportArray;
}
+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
    	return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
    	return NO;
    
    return YES;
}
@end
