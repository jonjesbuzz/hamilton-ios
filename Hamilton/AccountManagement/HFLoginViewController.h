//
//  HFLoginViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HFLoginViewControllerDelegate
-(void)didFinishHandlingUser:(BOOL)loggedIn;
@end

@interface HFLoginViewController : UITableViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    __unsafe_unretained id <HFLoginViewControllerDelegate> _delegate;
}
@property (nonatomic, assign) IBOutlet id <HFLoginViewControllerDelegate> delegate;
@property (nonatomic, weak) UIPopoverController *presentingPopoverController;
@end
	