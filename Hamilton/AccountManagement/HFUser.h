//
//  HFUser.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFDatabaseProtocol.h"
@interface HFUser : NSObject
{
    NSString *_username;
    NSString *_firstName;
    NSString *_lastName;
    NSMutableArray *_accounts;
    BOOL _loggedIn;
    NSNumber *_netWorth;
    id<HFDatabaseProtocol> _database;
}
- (HFUser*)initWithUsername:(NSString*)username;
- (void)loginWithPassword:(NSString*)password callbackBlock:(void(^)(BOOL))callback;
- (NSArray*)reportArrayFromDate:(NSDate*)startDate toDate:(NSDate*)endDate;
+ (void)createNewUserWithUserDictionary:(NSDictionary*) userDictionary callbackBlock:(void(^)(BOOL))callback;
+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

@property(readonly, strong, nonatomic) NSString* username;
@property(readonly, strong, nonatomic) NSString* firstName;
@property(readonly, strong, nonatomic) NSString* lastName;
@property(readonly, strong, nonatomic) NSArray* accounts;
@property(readonly, strong, nonatomic) NSNumber *netWorth;
@property(readonly) BOOL loggedIn;
@end
