//
//  HFViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/7/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFViewController.h"
#import "HFRegisterViewController.h"
@interface HFViewController ()

@end

@implementation HFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.tintColor = HamiltonColor;
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)didFinishHandlingUser:(BOOL)loggedIn
{
    if (loggedIn) {
        HFUser *user = [HFSession sharedInstance].currentUser;
        if (user != nil)
        {
            [self performSegueWithIdentifier:@"loggedInSegue" sender:self];
        }
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *detailViewController = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"loginViewSegue"]) {
        ((HFLoginViewController*)(detailViewController.viewControllers[0])).delegate = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
           ((HFLoginViewController*)(detailViewController.viewControllers[0])).presentingPopoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
        }
        // here we set the ViewController to be delegate in
        // detailViewController
    }
    if ([segue.identifier isEqualToString:@"registerViewSegue"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            ((HFRegisterViewController*)(detailViewController.viewControllers[0])).presentingPopoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
        }
    }
    if ([[segue identifier] isEqualToString:@"loggedInSegue"]) {
        // Something to do for logged in.
    }
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return ((1 << toInterfaceOrientation) & self.supportedInterfaceOrientations) != 0;
}
@end
