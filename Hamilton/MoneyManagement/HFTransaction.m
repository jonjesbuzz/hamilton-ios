//
//  HFTransaction.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFTransaction.h"
#import "HFSession.h"
#import "HFAccount.h"
#import DATABASE_TYPE_HEADER
@interface HFTransaction()
{
}
@end
@implementation HFTransaction

@synthesize name = _name;
@synthesize amount = _amount;
@synthesize category = _category;
@synthesize enteredDate = _enteredDate;
@synthesize transactionDate = _transactionDate;
@synthesize transactionID = _transactionID;

- (HFTransaction*)initWithTransactionDictionary:(NSDictionary*) transaction transactionID:(NSString*)transactionID
{
    if (transaction != nil)
    {
        if ([transaction valueForKey:@"source"] != nil)
        {
            _name = transaction[@"source"];
            _amount = [NSNumber numberWithDouble:[transaction[@"amount"] doubleValue]];
            _category = @"";
            if (transaction[@"sourceCategory"] != nil) {
                _category = transaction[@"sourceCategory"];
            }
            _enteredDate = [NSDate dateWithTimeIntervalSince1970:[transaction[@"dateEntered"] longValue] / ANDROID_DATE_SCALE];
            _transactionDate = [NSDate dateWithTimeIntervalSince1970:[transaction[@"transactionDate"] longValue] / ANDROID_DATE_SCALE];
            _transactionID = transactionID;
            return self;
        }
        else if ([transaction valueForKey:@"reason"] != nil)
        {
            _name = transaction[@"reason"];
            _amount = [NSNumber numberWithDouble:[transaction[@"amount"] doubleValue]];
            _category = transaction[@"expenseCategory"];
            _enteredDate = [NSDate dateWithTimeIntervalSince1970:[transaction[@"dateEntered"] longValue] / ANDROID_DATE_SCALE];
            _transactionDate = [NSDate dateWithTimeIntervalSince1970:[transaction[@"transactionDate"] longValue] / ANDROID_DATE_SCALE];
            _transactionID = transactionID;
            return self;
        }
    }
    return nil;
}

+ (HFTransaction*)createAndStoreNewTransaction:(NSDictionary*) transaction forAccount:(HFAccount*)account
{
    return [[[DATABASE_TYPE alloc] init] newTransactionFromDictionary:transaction forAccount:account];
}
- (void)deleteTransactionForAccount:(HFAccount*)account callback:(void(^)(BOOL))callback
{
    [[[DATABASE_TYPE alloc] init] deleteTransaction:self forAccount:account completion:callback];
}

- (BOOL)isEqualToTransaction:(HFTransaction*)other
{
    return [_name isEqualToString:other.name] && [_amount isEqualToNumber:other.amount] &&
    [_category isEqualToString:other.category] && [_enteredDate isEqualToDate:other.enteredDate] &&
    [_transactionDate isEqualToDate:other.transactionDate];
}

@end
