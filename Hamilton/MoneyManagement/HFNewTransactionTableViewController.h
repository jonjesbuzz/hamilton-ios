//
//  HFNewTransactionTableViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/11/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFAccount.h"
@protocol HFNewTransactionDelegate
-(void)didFinishAddingTransaction;
@end
@interface HFNewTransactionTableViewController : UITableViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *transactionNameField;
    IBOutlet UISegmentedControl *spendingTypeControl;
    IBOutlet UITextField *amountNameField;
    IBOutlet UILabel *currencyLabel;
    IBOutlet UITextField *categoryField;
    IBOutlet UIDatePicker *datePicker;
    
    __unsafe_unretained id <HFNewTransactionDelegate> _delegate;
}
@property (nonatomic) NSInteger *accountIndex;
@property(strong, nonatomic) HFAccount *account;
@property (nonatomic, assign) IBOutlet id <HFNewTransactionDelegate> delegate;
@end
