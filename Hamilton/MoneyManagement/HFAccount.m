//
//  HFAccount.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFAccount.h"
#import "HFTransaction.h"
#import DATABASE_TYPE_HEADER

@interface HFAccount()
//@property(strong, nonatomic) Firebase *accountFirebase;
@end
@implementation HFAccount
@synthesize accountName = _accountName;
@synthesize transactions = _transactions;
- (HFAccount*)initWithAccountDictionary:(NSDictionary*)accountDictionary
{
    self = [super init];
    if (accountDictionary != nil) {
        _accountName = accountDictionary[@"fullName"];
        if (_accountName == nil)
            return nil;
        // Create Transactions
        NSDictionary *transactionsRef = accountDictionary[@"transactions"];
        _balance = accountDictionary[@"balance"];
        _transactions = [[NSMutableArray alloc] initWithCapacity:[transactionsRef count]];
        for (NSString *dictKey in transactionsRef) {
            HFTransaction *currTrans = [[HFTransaction alloc] initWithTransactionDictionary:transactionsRef[dictKey] transactionID:dictKey];
            if (currTrans != nil) {
                [_transactions addObject:currTrans];
            }
        }
        return self;
    }
    return nil;
}
- (void)deleteAccountWithCompletionHandler:(void(^)(BOOL))callback
{
    [[[DATABASE_TYPE alloc] init] deleteAccount:self forUser:[HFSession sharedInstance].currentUser withCompletionHandler:callback];
}
+ (void)createNewAccount:(NSDictionary*) accountDictionary
{
    [[[DATABASE_TYPE alloc] init] newAccountWithDictionary:accountDictionary forUser:[HFSession sharedInstance].currentUser];
}
- (void)setBalance:(NSNumber *)balance
{
    [[[DATABASE_TYPE alloc] init] setBalance:balance forAccount:self];
}
+(NSString*) typeStringForAccountType:(HFAccountType)accountTypeIn {
    if (accountTypeIn == HFAccountTypeChecking)
        return @"checking";
    if (accountTypeIn == HFAccountTypeRetirement)
        return @"retirement";
    if (accountTypeIn == HFAccountTypeSavings)
        return @"savings";
    return nil;
}

@end
