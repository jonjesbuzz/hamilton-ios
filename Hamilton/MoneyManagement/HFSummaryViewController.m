//
//  HFSummaryViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFSummaryViewController.h"
#import "HFAccount.h"
#define ARC4RANDOM_MAX      0x100000000
#define RANDOM ((double)arc4random() / ARC4RANDOM_MAX)
@interface HFSummaryViewController ()
{
    HFUser *user;
    XYPieChart *pie;
    NSArray *pieColors;
}
@end

@implementation HFSummaryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLayoutSubviews
{
    if (pie == nil) {
        pie = [[XYPieChart alloc] initWithFrame:CGRectMake(0, 0, pieView.frame.size.width, pieView.frame.size.height)];
        [pieView addSubview:pie];
        [pie setShowPercentage:NO];
        [pie setDataSource:self];
        [pie setDelegate:self];
        [pie setBackgroundColor:[UIColor clearColor]];
        [pie reloadData];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    pieColors = [NSArray arrayWithObjects:
                 [UIColor colorWithRed:228/255.0 green:26/255.0 blue:28/255.0 alpha:1],
                 [UIColor colorWithRed:55/255.0 green:126/255.0 blue:184/255.0 alpha:1],
                 [UIColor colorWithRed:77/255.0 green:175/255.0 blue:74/255.0 alpha:1],
                 [UIColor colorWithRed:152/255.0 green:78/255.0 blue:163/255.0 alpha:1],
                 [UIColor colorWithRed:255/255.0 green:127/255.0 blue:0/255.0 alpha:1],
                 
                 [UIColor colorWithRed:127/255.0 green:127/255.0 blue:51/255.0 alpha:1],
                 [UIColor colorWithRed:166/255.0 green:86/255.0 blue:40/255.0 alpha:1],
                 [UIColor colorWithRed:247/255.0 green:129/255.0 blue:191/255.0 alpha:1],
                 [UIColor blackColor],nil];
    self.view.tintColor = HamiltonColor;
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    self.tabBarController.tabBar.tintColor = HamiltonColor;
    user = [HFSession sharedInstance].currentUser;
    nameLabel.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    accountBalanceLabel.text = [NSString stringWithFormat:@"%@", [self getCurrencyForNumber:user.netWorth]];
    numAccountsLabel.text = [NSString stringWithFormat:@"%lu accounts", (unsigned long)[user.accounts count]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAccountSummary) name:HFFirebaseUpdatedNotification object:nil];
    //[pie reloadData];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)firebaseUpdated
{
    [self updateAccountSummary];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAccountSummary) name:HFFirebaseUpdatedNotification object:nil];
    [self updateAccountSummary];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)updateAccountSummary
{
    [pie reloadData];
    accountBalanceLabel.text = [self getCurrencyForNumber:user.netWorth];
    acrossLabel.text = @"across";
    numAccountsLabel.text = [NSString stringWithFormat:@"%lu accounts", (unsigned long)[user.accounts count]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSString*)getCurrencyForNumber:(NSNumber*)number
{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyStyle stringFromNumber:number];
}
- (IBAction)logout:(id)sender {
    [[HFSession sharedInstance] destroySession];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return [user.accounts count];
}
- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    if (((HFAccount*)(user.accounts[index])).balance.doubleValue <= 0 || user.netWorth.doubleValue <= 0)
        return 0;
    if (abs(user.netWorth.doubleValue) < 0.001)
        return 0;
    return ((HFAccount*)(user.accounts[index])).balance.doubleValue / user.netWorth.doubleValue;
}
- (UIColor*)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return pieColors[index % user.accounts.count];
}
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    accountBalanceLabel.text = [self getCurrencyForNumber:[user.accounts[index] balance]];
    acrossLabel.text = @"in";
    numAccountsLabel.text = [user.accounts[index] accountName];
}
- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
    [self updateAccountSummary];
}
- (NSString*)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index
{
    return ((HFAccount*)(user.accounts[index])).accountName;
}
@end
