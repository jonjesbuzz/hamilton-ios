//
//  HFSummaryViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
@interface HFSummaryViewController : UIViewController <XYPieChartDataSource, XYPieChartDelegate>
{
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *accountBalanceLabel;
    IBOutlet UILabel *numAccountsLabel;
    IBOutlet UILabel *acrossLabel;
    IBOutlet UIView *pieView;
    
}
@end
