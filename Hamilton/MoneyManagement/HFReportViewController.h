//
//  HFReportViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/16/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
@interface HFReportViewController : UIViewController <XYPieChartDataSource, XYPieChartDelegate>
{
    IBOutlet UIView *pieChart;
    IBOutlet UILabel *amountLabel;
    IBOutlet UILabel *acrossInLabel;
    IBOutlet UILabel *categoryCountLabel;
    IBOutlet UIView *pieView;
}
@property(strong, nonatomic) NSDate *beginDate;
@property(strong, nonatomic) NSDate *endDate;
@end
