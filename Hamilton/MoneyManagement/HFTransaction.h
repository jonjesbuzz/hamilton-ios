//
//  HFTransaction.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFAccount.h"
@interface HFTransaction : NSObject
{
    NSString *_name;
    NSNumber *_amount;
    NSString *_category;
    NSDate *_enteredDate;
    NSDate *_transactionDate;
    NSString *_transactionID;
}
- (HFTransaction*)initWithTransactionDictionary:(NSDictionary*) transaction transactionID:(NSString*)transactionID;
+ (HFTransaction*)createAndStoreNewTransaction:(NSDictionary*) transaction forAccount:(HFAccount*)account;
- (void)deleteTransactionForAccount:(HFAccount*)account callback:(void(^)(BOOL))callback;
@property(readonly, strong, nonatomic) NSString* name;
@property(readonly, strong, nonatomic) NSNumber *amount;
@property(readonly, strong, nonatomic) NSString *category;
@property(readonly, strong, nonatomic) NSDate *enteredDate;
@property(readonly, strong, nonatomic) NSDate *transactionDate;
@property(readonly, strong, nonatomic) NSString* transactionID;
@end
