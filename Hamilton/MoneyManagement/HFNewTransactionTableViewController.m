//
//  HFNewTransactionTableViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/11/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFNewTransactionTableViewController.h"
#import "HFTransaction.h"
@interface HFNewTransactionTableViewController ()

@end
@implementation HFNewTransactionTableViewController
@synthesize account;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [transactionNameField performSelector:@selector(becomeFirstResponder) withObject:transactionNameField afterDelay:0.05];
    self.view.tintColor = HamiltonColor;
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    spendingTypeControl.tintColor = [UIColor redColor];
    amountNameField.textColor = [UIColor redColor];
    currencyLabel.textColor = [UIColor redColor];
    transactionNameField.delegate = amountNameField.delegate = categoryField.delegate = self;
    [datePicker setMaximumDate:[NSDate date]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == transactionNameField) {
        [transactionNameField resignFirstResponder];
        [amountNameField becomeFirstResponder];
    }
    else if (textField == amountNameField) {
        [amountNameField resignFirstResponder];
        [categoryField becomeFirstResponder];
    }
    else if (textField == categoryField) {
        [categoryField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == amountNameField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (IBAction)createNewTransaction:(id)sender {
    if ([transactionNameField.text isEqualToString:@""])
    {
        UIAlert(@"Transaction Name Required", @"Enter a name for this transaction.");
        return;
    }
    else if ([amountNameField.text isEqualToString:@""]) {
        UIAlert(@"Transaction Amount Requred", @"Enter an amount for this transaction.");
        return;
    }
    else if ([categoryField.text isEqualToString:@""]) {
        UIAlert(@"Transaction Category Required", @"Enter a category for this transaction.");
        return;
    }
    NSMutableDictionary *transaction = [[NSMutableDictionary alloc] initWithCapacity:7];
    NSNumber *amount = [NSNumber numberWithDouble:amountNameField.text.doubleValue * (spendingTypeControl.selectedSegmentIndex * 2 - 1)];
    if (account.balance.doubleValue + amount.doubleValue < 0) {
        [[[UIAlertView alloc] initWithTitle:@"Overdraft Alert" message:@"You cannot overdraft your account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if (fabs([amount doubleValue]) <= 0) {
        UIAlert(@"$0 Transaction", @"You cannot have a transaction of zero dollars!");
        return;
    }
    [transaction setValue:amount forKey:@"amount"];
    if ([amount doubleValue] < 0) {
        [transaction setValue:transactionNameField.text forKey:@"reason"];
        [transaction setValue:categoryField.text forKey:@"expenseCategory"];
    }
    else if ([amount doubleValue] > 0) {
        [transaction setValue:transactionNameField.text forKey:@"source"];
        [transaction setValue:categoryField.text forKey:@"sourceCategory"];
    }
    [transaction setValue:[NSNumber numberWithLong:datePicker.date.timeIntervalSince1970 * ANDROID_DATE_SCALE] forKey:@"transactionDate"];
    [transaction setValue:[NSNumber numberWithLong:[NSDate date].timeIntervalSince1970 * ANDROID_DATE_SCALE] forKey:@"dateEntered"];
    [HFTransaction createAndStoreNewTransaction:transaction forAccount:account];
    [self dismissNewTransactionView:sender];
}
- (IBAction)dismissNewTransactionView:(id)sender {
    if ([self delegate]) {
        [self.delegate didFinishAddingTransaction];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)spendTypeChanged:(id)sender {
    if (spendingTypeControl.selectedSegmentIndex == 0) {
        spendingTypeControl.tintColor = [UIColor redColor];
        amountNameField.textColor = [UIColor redColor];
        currencyLabel.textColor = [UIColor redColor];
    }
    else {
        spendingTypeControl.tintColor = HamiltonColor;
        amountNameField.textColor = [UIColor blackColor];
        currencyLabel.textColor = [UIColor blackColor];
    }
}

@end
