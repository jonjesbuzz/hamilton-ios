//
//  HFReportSelectorController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/16/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFReportSelectorController : UITableViewController
{
    IBOutlet UIDatePicker *datePicker;
    
}
- (IBAction)datePickerChangedValue:(id)sender;
@end
