//
//  HFAccount.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, HFAccountType) {
    HFAccountTypeSavings = 0,
    HFAccountTypeChecking = 1,
    HFAccountTypeRetirement = 2
};
@interface HFAccount : NSObject
{
    NSString *_accountName;
    NSMutableArray *_transactions;
    NSNumber *_balance;
}
- (HFAccount*) initWithAccountDictionary:(NSDictionary*)accountDictionary;
- (void)setBalance:(NSNumber*)balance;
+ (void)createNewAccount:(NSDictionary*) accountDictionary;
- (void)deleteAccountWithCompletionHandler:(void(^)(BOOL))callback;
+ (NSString*)typeStringForAccountType:(HFAccountType)accountTypeIn;
@property(readonly, strong, nonatomic) NSString *accountName;
@property(readonly, strong, nonatomic) NSArray *transactions;
@property(readonly, strong, nonatomic) NSNumber *balance;
@end
