//
//  HFReportViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/16/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFReportViewController.h"
#import "HFSession.h"
#import "HFTransaction.h"
@interface HFReportViewController ()
{
    XYPieChart *pie;
    NSArray *dataSource;
    NSMutableDictionary *categoryVal;
    
    NSArray *keys;
    NSNumber *total;
}
@end

@implementation HFReportViewController
@synthesize beginDate;
@synthesize endDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    total = @0;
    NSArray *report = [[HFSession sharedInstance].currentUser reportArrayFromDate:beginDate toDate:endDate];
    categoryVal = [NSMutableDictionary dictionary];
    for (HFTransaction *transaction in report)
    {
        NSString *key = transaction.category;
        CGFloat amount = transaction.amount.doubleValue;
        total = [NSNumber numberWithDouble:fabs(total.doubleValue) + fabs(amount)];
        NSNumber *currentAmount = [categoryVal valueForKey:key];
        if (currentAmount == nil) {
            currentAmount = [NSNumber numberWithDouble:fabs(amount)];
        } else {
            currentAmount = [NSNumber numberWithDouble:fabs(currentAmount.doubleValue) + fabs(amount)];
        }
        [categoryVal setValue:currentAmount forKey:key];
    }
    keys = [categoryVal allKeys];
    // Do any additional setup after loading the view.
}
- (void)viewWillLayoutSubviews
{
    if (pie == nil) {
        pie = [[XYPieChart alloc] initWithFrame:CGRectMake(0, 0, pieView.frame.size.width, pieView.frame.size.height)];
        [pieView addSubview:pie];
        [pie setShowPercentage:NO];
        [pie setDataSource:self];
        [pie setDelegate:self];
        [pie setBackgroundColor:[UIColor clearColor]];
        [pie reloadData];
    }
    amountLabel.text = [self getCurrencyForNumber:total];
    acrossInLabel.text = @"across";
    categoryCountLabel.text = [NSString stringWithFormat:@"%lu categories", (unsigned long)keys.count];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismissViewer:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSString*)getCurrencyForNumber:(NSNumber*)number
{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyStyle stringFromNumber:number];
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return [keys count];
}
- (UIColor*)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    NSUInteger hashCode = [keys[index] hash];
    srand48(hashCode);
    UIColor *color = [UIColor colorWithRed:abs(lrand48() % 255)/255.0 green:fabs((lrand48()) % 255)/255.0 blue:fabs((lrand48()) %  255)/255.0 alpha:1.0];
    return color;
}

-(CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    CGFloat flVal = fabs([categoryVal[keys[index]] doubleValue]);
    CGFloat totalVal = fabs(total.doubleValue);
    return flVal / totalVal;
}
-(NSString*)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index
{
    return [keys[index] isEqualToString:@""] ? @"(no category)" : keys[index];
}
-(void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    amountLabel.text = [self getCurrencyForNumber:[NSNumber numberWithDouble:fabs([categoryVal[keys[index]] doubleValue])] ];
    acrossInLabel.text = @"in";
    categoryCountLabel.text = keys[index];
}
-(void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
    amountLabel.text = [self getCurrencyForNumber:total];
    acrossInLabel.text = @"across";
    categoryCountLabel.text = [NSString stringWithFormat:@"%lu categories", (unsigned long)keys.count];
}
@end
