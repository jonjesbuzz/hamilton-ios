//
//  HFNewAccountTableViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/10/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFNewAccountTableViewController.h"
#import "HFAccount.h"
@interface HFNewAccountTableViewController ()

@end
@implementation HFNewAccountTableViewController
@synthesize delegate = _delegate;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    self.view.tintColor = HamiltonColor;
    [accountNameField performSelector:@selector(becomeFirstResponder) withObject:accountNameField afterDelay:0.05];
    accountNameField.delegate = accountDescriptionField.delegate = interestRateField.delegate = initialBalanceField.delegate = self;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == accountNameField) {
        [accountNameField resignFirstResponder];
        [accountDescriptionField becomeFirstResponder];
    }
    else if (textField == accountDescriptionField) {
        [accountDescriptionField resignFirstResponder];
        [interestRateField becomeFirstResponder];
    }
    else if (textField == interestRateField) {
        [interestRateField resignFirstResponder];
        [initialBalanceField becomeFirstResponder];
    }
    else if (textField == initialBalanceField) {
        [initialBalanceField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == initialBalanceField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    if (textField == interestRateField) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^([0-9]{1,2}|100)?(\\.([0-9]{1,6})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        
        if (numberOfMatches == 0 ||[newString doubleValue] > 100.00 || [newString doubleValue] < 0.00)
            return NO;

    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)makeNewAccount:(id)sender {
    if ([accountNameField.text isEqualToString:@""]) {
        UIAlert(@"Account Name Required", @"Enter a name for this account");
        return;
    }
    NSMutableDictionary *userDictionary = [[NSMutableDictionary alloc] init];
    NSNumber *interestRate = [NSNumber numberWithDouble:[interestRateField.text doubleValue]];
    NSNumber *initialBalance = [NSNumber numberWithDouble:[initialBalanceField.text doubleValue]];
    [userDictionary setValue:accountNameField.text forKey:@"accountName"];
    [userDictionary setValue:accountDescriptionField.text forKey:@"description"];
    [userDictionary setValue:interestRate forKey:@"interestRate"];
    [userDictionary setValue:[NSNumber numberWithLong:accountTypeSelector.selectedSegmentIndex] forKey:@"accountType"];
    [userDictionary setValue:initialBalance forKey:@"initialBalance"];
    
    [HFAccount createNewAccount:userDictionary];
    
    [self dismissNewAccountController:sender];
}
- (IBAction)dismissNewAccountController:(id)sender {
    if (self.delegate != nil)
    {
        [self.delegate didFinishAddingAccount];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
