//
//  HFTransactionTableViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFTransaction.h"
#import "HFAccount.h"
#import "HFNewTransactionTableViewController.h"
@interface HFTransactionTableViewController : UITableViewController <HFNewTransactionDelegate, UIAlertViewDelegate>
@property(strong, nonatomic) HFAccount *account;
@end
