//
//  HFAccountsTableViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFNewAccountTableViewController.h"
@interface HFAccountsTableViewController : UITableViewController <HFNewAccountDelegate>

@end
