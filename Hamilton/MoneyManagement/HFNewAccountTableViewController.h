//
//  HFNewAccountTableViewController.h
//  Hamilton
//
//  Created by Jonathan Jemson on 3/10/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HFNewAccountDelegate
-(void)didFinishAddingAccount;
@end
@interface HFNewAccountTableViewController : UITableViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *accountNameField;
    IBOutlet UITextField *accountDescriptionField;
    IBOutlet UITextField *interestRateField;
    IBOutlet UITextField *initialBalanceField;
    IBOutlet UISegmentedControl *accountTypeSelector;
    
    __unsafe_unretained id <HFNewAccountDelegate> _delegate;
}
@property (nonatomic, assign) IBOutlet id <HFNewAccountDelegate> delegate;
@end
