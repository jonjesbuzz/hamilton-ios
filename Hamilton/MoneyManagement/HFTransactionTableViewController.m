//
//  HFTransactionTableViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFTransactionTableViewController.h"
#import "HFNewTransactionTableViewController.h"
#import "HFListingTableViewCell.h"
#import "HFTransactionDetailViewController.h"
#define OverdraftingRemoveAlert 1
#define RemoveTransactionAlert 2
@interface HFTransactionTableViewController ()
{
    NSArray *transactions;
    NSIndexPath *_tempPath;
    HFTransaction *_tempTransaction;
}
@end

@implementation HFTransactionTableViewController
@synthesize account;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:HFFirebaseUpdatedNotification object:nil];
    transactions = [[self.account.transactions sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        HFTransaction *transaction1 = (HFTransaction*)obj1;
        HFTransaction *transaction2 = (HFTransaction*)obj2;
        return [transaction2.transactionDate compare:transaction1.transactionDate];
    }] mutableCopy];
    self.navigationItem.prompt = [NSString stringWithFormat:@"Balance: %@", [self getCurrencyForNumber:[self calculateBalance]]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentNewTransactionController:)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)refreshData
{
    self.account = [self getAccount];
    transactions = [[self.account.transactions sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        HFTransaction *transaction1 = (HFTransaction*)obj1;
        HFTransaction *transaction2 = (HFTransaction*)obj2;
        return [transaction2.transactionDate compare:transaction1.transactionDate];
    }] mutableCopy];
    self.navigationItem.prompt = [NSString stringWithFormat:@"Balance: %@", [self getCurrencyForNumber:[self calculateBalance]]];
    [self.tableView reloadData];

}
- (HFAccount*)getAccount
{
    for (HFAccount* acct in [[HFSession sharedInstance] currentUser].accounts){
        if ([acct.accountName isEqualToString:self.account.accountName]) {
            return acct;
        }
    }
    return nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [transactions count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HFListingTableViewCell *cell = (HFListingTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"transactionCell" forIndexPath:indexPath];
    HFTransaction *transaction = transactions[indexPath.row];
    cell.textLabel.text = transaction.name;
    
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    cell.detailTextLabel.text = [currencyStyle stringFromNumber:transaction.amount];
    if (transaction.amount.doubleValue < 0) {
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    else {
        cell.detailTextLabel.textColor = HamiltonColor;
    }
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        _tempPath = indexPath;
        HFTransaction *del = transactions[indexPath.row];
        _tempTransaction = del;
        if (self.account.balance.doubleValue - del.amount.doubleValue < 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Overdraft Alert" message:@"Rolling back this transaction causes your account to overdraft." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            alert.tag = OverdraftingRemoveAlert;
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rollback Transaction" message:@"You cannot undo this change." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Rollback", nil];
            [alert show];
            alert.tag = RemoveTransactionAlert;
        }
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Rollback";
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == OverdraftingRemoveAlert) {
        // Uncomment to allow Overdraft Reversing.
        
        //if (buttonIndex == 0) {
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rollback Transaction" message:@"You cannot undo this change." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Rollback", nil];
//            [alert show];
//            alert.tag = RemoveTransactionAlert;
//        }
    } else if (alertView.tag == RemoveTransactionAlert) {
        if (buttonIndex == 1) {
            [_tempTransaction deleteTransactionForAccount:self.account callback:nil];
            _tempTransaction = nil;
        }
    }
}
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"newTransactionModal"]) {
        UINavigationController *nextNav = [segue destinationViewController];
        HFNewTransactionTableViewController *next = nextNav.viewControllers[0];
        next.delegate = self;
        [next setAccount:account];
    }
    else if ([segue.identifier isEqualToString:@"pushToTransactionDetail"]) {
        HFTransactionDetailViewController *detail = [segue destinationViewController];
        detail.transaction = sender;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushToTransactionDetail" sender:transactions[indexPath.row]];
}
- (NSNumber*)calculateBalance
{
    NSNumber *balance = @0;
    for (HFTransaction *transaction in transactions)
    {
        balance = [NSNumber numberWithDouble:([balance doubleValue] + [transaction.amount doubleValue])];
    }
    [account setBalance:balance];
    return balance;
}

- (NSString*)getCurrencyForNumber:(NSNumber*)number
{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyStyle stringFromNumber:number];
}

- (void)presentNewTransactionController:(id)sender
{
    [self performSegueWithIdentifier:@"newTransactionModal" sender:self];
}
- (void)didFinishAddingTransaction
{
}
@end
