//
//  HFReportSelectorController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/16/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFReportSelectorController.h"
#import "HFReportViewController.h"
#define THIRTY_DAYS 2592000
#define JANUARY12014 1388534400
@interface HFReportSelectorController ()
{
    NSDate *startDate;
    NSDate *endDate;
    NSIndexPath *selectedRow;
}
@end

@implementation HFReportSelectorController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    startDate = [NSDate dateWithTimeIntervalSinceNow:-THIRTY_DAYS];
    endDate = [NSDate date];
    [datePicker setMaximumDate:[NSDate date]];
    [datePicker setMinimumDate:[NSDate dateWithTimeIntervalSince1970:JANUARY12014]];
    
    NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView cellForRowAtIndexPath:selectedCellIndexPath].detailTextLabel.text = [self formattedStringForDate:startDate];
    
    [self.tableView selectRowAtIndexPath:selectedCellIndexPath animated:false scrollPosition:UITableViewScrollPositionMiddle];
    [self tableView:self.tableView didSelectRowAtIndexPath:selectedCellIndexPath];
    
    selectedCellIndexPath= [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView cellForRowAtIndexPath:selectedCellIndexPath].detailTextLabel.text = [self formattedStringForDate:endDate];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row != 3);
}
#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = indexPath;
    if (indexPath.row == 0)
    {
        datePicker.date = startDate;
        [datePicker setMaximumDate:endDate];
        [datePicker setMinimumDate:[NSDate dateWithTimeIntervalSince1970:JANUARY12014]];
    }
    else if (indexPath.row == 1)
    {
        datePicker.date = endDate;
        [datePicker setMinimumDate:startDate];
        [datePicker setMaximumDate:[NSDate date]];
    }
    [self.tableView cellForRowAtIndexPath:selectedRow].detailTextLabel.text = [self formattedStringForDate:datePicker.date];
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    UINavigationController *nextNav = [segue destinationViewController];
    HFReportViewController *next = nextNav.viewControllers[0];
    next.beginDate = startDate;
    next.endDate = endDate;
    // Pass the selected object to the new view controller.
}

- (NSString*)formattedStringForDate:(NSDate*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterShortStyle;
    df.timeStyle = NSDateFormatterNoStyle;
    return [df stringFromDate:date];
}
- (IBAction)datePickerChangedValue:(id)sender {
    if (selectedRow.row == 0)
    {
        startDate = datePicker.date;
    }
    if (selectedRow.row == 1)
    {
        endDate = datePicker.date;
    }
    [self.tableView cellForRowAtIndexPath:selectedRow].detailTextLabel.text = [self formattedStringForDate:datePicker.date];
}
- (IBAction)showReportModal:(id)sender {
    [self performSegueWithIdentifier:@"showReportModal" sender:sender];
}
@end
