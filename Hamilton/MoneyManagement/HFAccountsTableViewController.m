//
//  HFAccountsTableViewController.m
//  Hamilton
//
//  Created by Jonathan Jemson on 3/8/14.
//  Copyright (c) 2014 Nine Rupees. All rights reserved.
//

#import "HFAccountsTableViewController.h"
#import "HFTransactionTableViewController.h"
#import "HFListingTableViewCell.h"
#import "HFAccount.h"
#define RemoveAccountAlert 1
@interface HFAccountsTableViewController ()
{
    HFUser *currentUser;
    NSArray *accountListing;
    NSIndexPath *_tempPath;
    HFAccount *_tempAccount;
}
@end

@implementation HFAccountsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:HFFirebaseUpdatedNotification object:nil];
    self.navigationController.navigationBar.tintColor = HamiltonColor;
    currentUser = [HFSession sharedInstance].currentUser;
    //accountListing = [self getSortedAccountArray];
    // UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    // [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    // self.refreshControl = refreshControl;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:HFFirebaseUpdatedNotification object:nil];
    [self refreshData];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFFirebaseUpdatedNotification object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [accountListing count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HFListingTableViewCell *cell = (HFListingTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"accountCell" forIndexPath:indexPath];
    
    cell.textLabel.text = ((HFAccount*)accountListing[indexPath.row]).accountName;
    cell.detailTextLabel.text = [self getCurrencyForNumber:((HFAccount*)accountListing[indexPath.row]).balance];
    if (((HFAccount*)accountListing[indexPath.row]).balance.doubleValue < 0)
    {
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    else {
        cell.detailTextLabel.textColor = HamiltonColor;
    }
    
    return cell;
}

-(void)refreshData
{
    accountListing = [self getSortedAccountArray];
    [[self tableView] reloadData];
    //  [self.refreshControl endRefreshing];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        _tempPath = indexPath;
        HFAccount *del = accountListing[indexPath.row];
        _tempAccount = del;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Account" message:@"All account and transaction data will be deleted.  You cannot undo this change." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        [alert show];
        alert.tag = RemoveAccountAlert;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Delete";
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == RemoveAccountAlert) {
        if (buttonIndex == 1) {
            [_tempAccount deleteAccountWithCompletionHandler:nil];
            _tempAccount = nil;
        }
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"pushToTransactions"]) {
        HFAccount *account = (HFAccount*) sender;
        HFTransactionTableViewController *next = [segue destinationViewController];
        next.account = account;
    }
    else if ([segue.identifier isEqualToString:@"modalAddAccount"]){
        UINavigationController *nextNav = [segue destinationViewController];
        HFNewAccountTableViewController *next = nextNav.viewControllers[0];
        next.delegate = self;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushToTransactions" sender:accountListing[indexPath.row]];
}
- (NSArray*) getSortedAccountArray
{
    NSArray *rawAccountListing = [[currentUser accounts] mutableCopy];
    rawAccountListing = [rawAccountListing sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        HFAccount *account1 = (HFAccount*)obj1;
        HFAccount *account2 = (HFAccount*)obj2;
        return [account1.accountName compare:account2.accountName];
    }];
    return rawAccountListing;
}
- (NSString*)getCurrencyForNumber:(NSNumber*)number
{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyStyle stringFromNumber:number];
}
- (void)didFinishAddingAccount
{
    //[self.refreshControl beginRefreshing];
    [self refreshData];
}
@end
